require 'squib'

cards = Squib.csv(file: 'src/cards.csv')
# Stage,White,Blue,Green,Red,Black,VP,Gen-White,Gen-Blue,Gen-Green,Gen-Red,Gen-Black

def cost_of(cards, index)
  0.upto(cards['Stage'].size-1).map do |i|
    [ cards['WhiteCost'][i],
      cards['BlueCost' ][i],
      cards['GreenCost'][i],
      cards['RedCost'  ][i],
      cards['BlackCost'][i],
    ].compact[index] || ""
  end
end

def images(embed)
  embed.png key: 'W', file: 'src/white.png', height: 64, width: 64
  embed.png key: 'U', file: 'src/blue.png' , height: 64, width: 64
  embed.png key: 'G', file: 'src/green.png', height: 64, width: 64
  embed.png key: 'R', file: 'src/red.png'  , height: 64, width: 64
  embed.png key: 'B', file: 'src/black.png', height: 64, width: 64
end

Squib::Deck.new(cards: 5 || cards['Stage'].size, layout: 'layout.yml', width: 597, height: 822) do
  background color: 'white'
  png file: 'src/template-mini.png'
  rect layout: 'cut'
  rect layout: 'safe'

  build :card do
    text str: cards['VP'],         layout: :vp
    text str: cards['Production'], layout: :product
    text str: cost_of(cards, 0).reverse,   layout: :cost0 do |embed| images(embed) end
    text str: cost_of(cards, 1).reverse,   layout: :cost1 do |embed| images(embed) end
    text str: cost_of(cards, 2).reverse,   layout: :cost2 do |embed| images(embed) end
    #text str: cost_of(cards, 3).reverse,   layout: :cost3 do |embed| images(embed) end

    save prefix: 'card_', format: :png
  end

  build :cardback do
    # Don't forget background here to reset the card ;)
    background color: 'white'
    text str: cards['Stage'], layout: :cardback, font: 'Serif bold 256'
    save prefix: 'cardback_', format: :png
  end
end


nobles = Squib.csv(file: 'src/nobles.csv')
